package slick;

import objetos.Botao;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.*;
import static org.newdawn.slick.Input.*;
import org.newdawn.slick.state.*;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import static slick.Game.play;
import static slick.Game.scores;



public abstract class Menu extends BasicGameState {
     
    private Botao botao_jogar;
    private Botao botao_scores;
    public static TrueTypeFont fonte;
    public static TrueTypeFont titulo;
    private Image img;
    
    
    public Menu(int state) {}
    
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
        
        fonte = new TrueTypeFont(new java.awt.Font("Berlin Sans FB", java.awt.Font.BOLD, 30), false);
        titulo = new TrueTypeFont(new java.awt.Font("Cooper Black", java.awt.Font.PLAIN , 45), false);
        
        this.botao_jogar = new Botao(sbg, (1.5f /4), (4.8f /8), 1f /4, 1f /8, "Jogar");
        this.botao_scores = new Botao(sbg, (1.5f /4), (6f /8), 1f /4, 1f /8, "High Scores");
        img = new Image("imagens/titulo.png");
        
    }
    
    public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException{
        
        if (Display.wasResized()) GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
        this.botao_jogar.render(g);
        this.botao_scores.render(g);
        img.draw((Game.width - img.getWidth())/2, (Game.height - img.getHeight())/4);
    }
    
    public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
        Input input = gc.getInput();
        if (input.isKeyDown(Input.KEY_ENTER)){
            
            sbg.getState(play).init(gc, sbg);
            sbg.enterState(play, new FadeOutTransition(Color.black), new FadeInTransition(Color.black));
        }
        
        int xM = Mouse.getX();
        int yM = Mouse.getY();
        
        boolean taDentro1 = false;
        boolean taDentro2 = false;
        
        if (this.botao_jogar.getRect().contains(xM, Display.getHeight() - yM)) {
            taDentro1 = true;
            if (input.isMouseButtonDown(MOUSE_LEFT_BUTTON)) {
                sbg.getState(play).init(gc, sbg);
                sbg.enterState(play, new FadeOutTransition(Color.black), new FadeInTransition(Color.black));
            }
        }
        
        if (this.botao_scores.getRect().contains(xM, Display.getHeight() - yM)) {
            taDentro2 = true;
            if (input.isMouseButtonDown(MOUSE_LEFT_BUTTON)) {
                sbg.getState(scores).init(gc, sbg);
                sbg.enterState(scores, new FadeOutTransition(Color.black), new FadeInTransition(Color.black));
            }
        }
        
        this.botao_jogar.mudaCor(taDentro1);
        this.botao_scores.mudaCor(taDentro2);
    }
    
    public int getID() {
        return 0;
    }
}