package slick;

import java.io.*;
import objetos.ListadeAster;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import static slick.Game.menu;
import static slick.Menu.fonte;
import static slick.Menu.titulo;


public class Scores extends BasicGameState{
    
    private int qtdd_scores;
    private int score_height;
    private long[] scores;
    
    public Scores(int state) {
    
    }
    
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
        
        this.qtdd_scores = 5;
        this.score_height = 30;
        scores = new long[this.qtdd_scores];
        titulo = new TrueTypeFont(new java.awt.Font("Cooper Black", java.awt.Font.PLAIN , 45), false);
        fonte = new TrueTypeFont(new java.awt.Font("Berlin Sans FB", java.awt.Font.BOLD, this.score_height), false);
        
        String fileName = "scores.txt";
        String line = null;

        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            
            for (int i = 0; i < this.qtdd_scores; i++) {
                line = bufferedReader.readLine();
                if (line != null) {
                scores[i] = Long.parseLong(line);
                    
                }
            }

            bufferedReader.close();
        }
        catch (FileNotFoundException ex) {
            System.out.println(
                "Nao pode abrir o arquivo '" + fileName + "'");                
        }
        catch (IOException ex) {
            System.out.println(
                "Erro lendo o arquivo '" + fileName + "'");         
        }
        
        long score = ListadeAster.getPontuacao();
        int pos = -1;
        for (int i = 0; i < this.qtdd_scores; i++) {
            if (score >= scores[i]) {
                pos = i;
                break;
            }
        }
        
        if (pos != -1) {
            long t1 = -1, t2 = -1;
            for(int i = 0; i < this.qtdd_scores; i++) {
                if (i == pos) {
                    t1 = scores[i];
                    scores[i] = score;
                }
                else if (i > pos) {
                    t2 = scores[i];
                    scores[i] = t1;
                    t1 = t2;
                }
            }
        }
        

        try {
            FileWriter fileWriter = new FileWriter(fileName);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            
             for (int i = 0; i < this.qtdd_scores; i++)
                bufferedWriter.write(String.valueOf(scores[i]) + "\n");
            
            bufferedWriter.close();
        }
        catch (IOException ex) {
            System.out.println(
                "Erro escrevendo o arquivo '" + fileName + "'");
        }
        
        ListadeAster.setPontuacao(0);
    }
    
    public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
        
        if (Display.wasResized()) GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
        
        g.setFont(fonte);
        g.setColor(new Color(255, 255, 255));
        for (int i = 0; i < this.qtdd_scores; i++) {
            String str = String.valueOf(i+1) + "º. " + String.valueOf(scores[i]);
            g.drawString(str, (Game.width - fonte.getWidth(str))/2, (Game.height - (2 * this.qtdd_scores - 1) * fonte.getHeight(str))/2 + 2*i*fonte.getHeight(str));
        }
    }
    
    public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException {
        
        Input input = gc.getInput();
        if (input.isKeyDown(Input.KEY_ENTER)){
            
            sbg.getState(menu).init(gc, sbg);
            sbg.enterState(menu, new FadeOutTransition(Color.black), new FadeInTransition(Color.black));
        }
        
    }
    
    public int getID(){
        
        return 2;
    }
}