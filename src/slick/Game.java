package slick;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

public class Game extends StateBasedGame {
    
    public static final String gamename = "Asteroids";
    public static final int menu = 0;
    public static final int play = 1;
    public static final int scores = 2;
    public static int width;
    public static int height;
    
    public Game(String gamename) {
        
        super(gamename);
        this.addState(new Menu(menu) {});
        this.addState(new Play(play));
        this.addState(new Scores(scores));
    }

    public void initStatesList(GameContainer gc) throws SlickException {
        
        this.getState(menu).init(gc, this);
        this.getState(play).init(gc, this);
        this.getState(scores).init(gc, this);
        this.enterState(menu);
    }

    public static void main(String[] args) throws LWJGLException {
        
        AppGameContainer appgc;
        try {
            appgc = new AppGameContainer(new Game(gamename));
            
            width = 7 * appgc.getScreenWidth() / 8;
            height = 7 * appgc.getScreenHeight() / 8;
            
            appgc.setDisplayMode(width, height, false);
            appgc.setShowFPS(false);
            appgc.setTargetFrameRate(70);
            Display.setResizable(true);
            appgc.setUpdateOnlyWhenVisible(true);
            appgc.start();
        } catch (SlickException e) {
            
                e.printStackTrace();;
        }
    }
}