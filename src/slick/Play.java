package slick;

import objetos.*;
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;
import objetos.ListadeAster;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import static slick.Game.scores;

public class Play extends BasicGameState{
    
    Nave nave;
    private long timer;
    public ListadeAster asters;
    public ListadeVida vidas;
    private static long delta_time;
    private long StartTime;
    private boolean reinicia = false;

    public Play(int state){
        
        
    }
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{
        
        this.asters = new ListadeAster();
        this.nave = new Nave(gc);
        this.asters.novo_aster(gc);
        this.vidas = new ListadeVida(gc);
        this.StartTime = System.currentTimeMillis();
        asters.setPontuacao(0);
        asters.setStreak(0);
        
    }
    
    public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException{
        
        if (Display.wasResized()) GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
        g.setColor(Color.green);
        String strPontos = String.valueOf(asters.getPontuacao());
        g.drawString("PONTOS: " + strPontos, gc.getWidth()/2 - 20,  10);
        g.setColor(Color.cyan);
        String strStreak = String.valueOf(asters.getStreak());
        g.drawString("STREAK: " + strStreak, gc.getWidth() * 4/5,  10);
        this.nave.tiros.render(gc, g);
        this.nave.render(gc, g);
        this.asters.render(gc, g);
        this.vidas.render(gc, g);
    }
    
    public void update(GameContainer gc, StateBasedGame sbg, int i)throws SlickException{
        this.timer += i;
        if(this.timer >= 1200){
            
            this.asters.novo_aster(gc);
            this.timer = 0;
        }
        if(asters.getStreak() == 15){
            
            nave.setTiroNormal(false);
        }
        
        this.nave.update(gc, i);
        this.nave.tiros.update();
        this.asters.update(gc);
        this.calcula_delta_time();
        this.vidas.update(gc);
        
        reinicia = Nave.getPerdeu();
        if(reinicia == true){
            reinicia = false;
            sbg.getState(scores).init(gc, sbg);
            sbg.enterState(scores, new FadeOutTransition(Color.black), new FadeInTransition(Color.black));
            
        }
    }
    
    public int getID(){
        
        return 1;
    }
    
    private void calcula_delta_time(){
        
        long now_time = System.currentTimeMillis();
        this.delta_time = now_time - StartTime;
        this.StartTime = now_time;
        if (delta_time == 0 ) this.delta_time +=1;
    }
    public static float get_delta_time(){
        
        return delta_time;
    }
    public static float get_delta_time_seconds(){
        
        return (float) (delta_time*0.1)/1000;
    }
    
}
