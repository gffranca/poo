package objetos;

import java.util.Random;
import java.util.ArrayList;
import static objetos.Asteroid.TAMANHO;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class ListadeAster {
    
    ArrayList<Asteroid> asters;
    private static long pontuacao;
    private static long streak;
    private int pontoAster;
    
    public ListadeAster(){
        this.pontoAster = 300;
        asters = new ArrayList();
    }
    
    public static long getPontuacao() {
        
        return pontuacao;
    }
    
    public static void setPontuacao(long valor) {
        pontuacao = valor;
    }
    
    public static long getStreak() {
        
        return streak;
    }
    
    public static void setStreak(long valor) {
        streak = valor;
    }
    
    public void novo_aster(GameContainer gc) throws SlickException{
        
        asters.add(new Asteroid(gc, 0));
    }
    
    public void update(GameContainer gc) throws SlickException{
        Runnable colisaoHandler = new ColisaoRunner(this, gc);
        Thread colisaoThread = new Thread(colisaoHandler);
        colisaoThread.start();
        
        for(int j = asters.size() -1; j > -1; j--){
            if (asters.get(j).fora_da_tela()) asters.remove(j);
            else asters.get(j).update();
        }
        try {
        colisaoThread.join();
        }
        catch (InterruptedException e){
            System.out.println(e.getMessage());
        }
    }
    
    public void render(GameContainer gc, Graphics g){
        
        for (int i = 0; i < asters.size(); i++){
            asters.get(i).render(gc,  g);
        }
    }
    
    public void colisao(GameContainer gc) throws SlickException{
        
        for(int j = asters.size() -1; j > -1; j--){
            
            if((asters.get(j).aster.contains(Nave.nave)) || (asters.get(j).aster.intersects(Nave.nave))){
                    
                asters.remove(j);
                j--;
                Nave.perdeVida();
            }
            
            for(int k = ListadeTiros.tiros.size() -1; k > -1; k--){
                if(j > -1 && ((asters.get(j).aster.contains(ListadeTiros.tiros.get(k).getShape())) || (asters.get(j).aster.intersects(ListadeTiros.tiros.get(k).getShape())))){
                    this.morreu(gc, asters.get(j).tamanho, asters.get(j).x, asters.get(j).y, asters.get(j).angulo);
                    pontuacao += (long) this.pontoAster / asters.get(j).tamanho;
                    streak++;
                    asters.remove(j);
                    ListadeTiros.tiros.remove(k);
                    break;
                }
            }
        }
    }
    
    protected void morreu(GameContainer gc, long tamMorto, float xMorto, float yMorto, float anguloMorto) throws SlickException{
        
        if(tamMorto < TAMANHO){
            Random random = new Random();
            
            this.novo_filho(gc, tamMorto + 1);
            float w = asters.get(asters.size() -1).aster.getMaxX() - asters.get(asters.size() -1).aster.getMinX();
            
            float ang = anguloMorto + (random.nextInt(15) + 15); 
            asters.get(asters.size() -1).angulo = ang;
            float xF = xMorto + w/2;
            asters.get(asters.size() -1).mudaPos(asters.get(asters.size() -1).aster, xF, yMorto);
            
            
            this.novo_filho(gc, tamMorto + 1);
            w = asters.get(asters.size() -1).aster.getMaxX() - asters.get(asters.size() -1).aster.getMinX();
            
            ang= anguloMorto - (random.nextInt(15) + 15); 
            asters.get(asters.size() -1).angulo = ang;
            xF = xMorto + w/2;
            asters.get(asters.size() -1).mudaPos(asters.get(asters.size() -1).aster, xF, yMorto);
        }
    }
    
    protected void novo_filho(GameContainer gc, long tam) throws SlickException{
        
        asters.add(new Asteroid(gc, tam));
    }
}