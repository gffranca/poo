/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;

/**
 *
 * @author eduar
 */
public class ColisaoRunner implements Runnable {
    private ListadeAster asteroides;
    private GameContainer gc;
    public ColisaoRunner(ListadeAster lista, GameContainer gc){
        asteroides = lista;
        this.gc = gc;
    }

    @Override
    public void run() {
        try {
        asteroides.colisao(gc);
        }
        catch (SlickException e){
            System.out.println(e.getMessage());
        }
    }
    
}
