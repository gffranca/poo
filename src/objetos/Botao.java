package objetos;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.*;
import org.newdawn.slick.state.StateBasedGame;
import slick.Game;
import static slick.Menu.fonte;


public class Botao {
    
    protected float x;
    protected float y;
    protected float w;
    protected float h;
    protected float r1;
    protected float r2;
    protected float r3;
    protected float r4;
    protected int red;
    protected int green;
    protected String texto;
    protected RoundedRectangle rectIn;
    protected RoundedRectangle rectOut;
    
    public Botao(StateBasedGame sbg, float x, float y, float w, float h, String texto){
        this.r1 = x;
        this.r2 = y;
        this.r3 = w;
        this.r4 = h;
        this.x = x * Game.width;
        this.y = y * Game.height;
        this.w = w * Game.width;
        this.h = h * Game.height;
        this.red = 255;
        this.green = 0;
        this.texto = texto;
        this.rectIn = new RoundedRectangle(this.x, this.y, this.w, this.h, 20);
        this.rectOut = new RoundedRectangle(this.x, this.y, this.w, this.h, 20);
    }
    
    public void render(Graphics g) {
        
        g.setFont(fonte);
        g.setColor(new Color(this.red, this.green, 0));
        float def = g.getLineWidth();
        g.setLineWidth(10);
        g.draw(this.rectOut);
        g.setLineWidth(def);
        g.setColor(new Color(255, 255, 255));
        g.fill(this.rectIn);
        g.draw(this.rectIn);
        g.setColor(new Color(0, 0, 0));
        g.drawString(texto, x + w/2 - fonte.getWidth(texto)/2, y + h/2 - fonte.getHeight(texto)/2);
        
    }
    
    public void mudaCor(boolean taDentro) {
        
        if (taDentro) {
            this.red = 0;
            this.green = 255;
        } else {
            this.red = 255;
            this.green = 0;
        }
    }

    public RoundedRectangle getRect() {
        RoundedRectangle rectAtual = new RoundedRectangle(this.r1 * Display.getWidth(), this.r2 * Display.getHeight(), this.r3 * Display.getWidth(), this.r4 * Display.getHeight(), 20);
        return rectAtual;
    }
}