package objetos;

import org.newdawn.slick.*;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.*;
import slick.Game;

public class Nave extends Obj{
    
    public ListadeTiros tiros;
    private long tempo_para_atirar;
    private final long delay_tiros = 201;
    protected static Shape nave;
    protected static int vida;
    private static boolean tomouDano;
    private static boolean perdeu;
    private static int time;
    private final float velocidadeAng;
    private static boolean tiroNormal = true;
    
    
    public Nave(GameContainer gc) throws SlickException{
        
        this.angulo = 0;
        nave = null;
        tomouDano = false;
        perdeu = false;
        vida = 3;
        time = 0;
        tiroNormal = true;
        this.velocidade = 10.4f;
        this.velocidadeAng = 0.7f * velocidade;
        this.tiros = new ListadeTiros();
        this.tempo_para_atirar = 0;
        float[] points = new float[]{0, 70, 25, 70, 35, 60, 45, 70, 70, 70, 35, 0};
        nave = new Polygon(points);
        this.x = Game.width/2;
        this.y = Game.height/2;
        this.mudaPos(nave, this.x, this.y);
    }
    
    public void render(GameContainer gc, Graphics g){
        
        this.colore(g, this.nave, 0, 0, 0);
        g.setLineWidth(2);
        if(tomouDano == true) this.desenha(gc, g, this.nave, 255, 0, 0);
        else this.desenha(gc, g, this.nave, 143, 60, 226);
        g.setLineWidth(1);
    }
    
    public void update(GameContainer gc, int i) throws SlickException{
        
        this.verificaMov(gc, i);
        if(tomouDano == true){
            
            time += i;
            if(time > 500){
                
                tomouDano = false;
                time = 0;
            }
        }
    }

    private void verificaMov(GameContainer gc, int i) throws SlickException{
        
        Input input = gc.getInput();
        if (input.isKeyDown(Input.KEY_A)){
            
            this.angulo -= this.velocidadeAng;
            this.nave = this.nave.transform(Transform.createRotateTransform((float)(Math.toRadians(- this.velocidadeAng)), this.nave.getCenterX(), this.nave.getCenterY()));
        }
        if (input.isKeyDown(Input.KEY_D)){
            
            this.angulo += this.velocidadeAng;
            this.nave = this.nave.transform(Transform.createRotateTransform((float)(Math.toRadians(this.velocidadeAng)) , this.nave.getCenterX(), this.nave.getCenterY()));
        }
        if (input.isKeyDown(Input.KEY_SPACE) && this.tempo_para_atirar > 200){
            if(tiroNormal){
                this.tiros.novo_tiro(gc, this.angulo, this.x, this.y);
                this.tempo_para_atirar = 0;
            }
            else{
                this.tiros.novo_tiro(gc, this.angulo, this.x, this.y);
                this.tiros.novo_tiro(gc, this.angulo - 15, this.x, this.y);
                this.tiros.novo_tiro(gc, this.angulo + 15, this.x, this.y);
                this.tempo_para_atirar = 0;
            }
        }
        if (this.tempo_para_atirar < this.delay_tiros) this.tempo_para_atirar += i;
        if (input.isKeyDown(Input.KEY_W)) this.move(this.nave);
        if (input.isKeyPressed(Input.KEY_V) && (ListadeAster.getPontuacao() >= 5000)){
            ganhaVida();
            ListadeAster.setPontuacao(ListadeAster.getPontuacao() - 5000);
        }
    }
    
    private void fora_da_tela(){
        float width = this.nave.getMaxX() - this.nave.getMinX();
        float height = this.nave.getMaxY() - this.nave.getMinY();
        if (this.x > Game.width){
            
            this.x = -width;
            this.nave.setCenterX(this.x);
        }
        if (this.x + width < 0){
            
            this.x = Game.width;
            this.nave.setCenterX(this.x);
        }
        if (this.y > Game.height){
            
            this.y = -height;
            this.nave.setCenterY(this.y);
        }
        if (this.y + height < 0){
            
            this.y = Game.height;
            this.nave.setCenterY(this.y);
        }
    }
    
    @Override
    protected void move(Shape nome){
   
        this.x += this.velocidadeAng * Math.sin(Math.toRadians(this.angulo));
        this.y -= this.velocidadeAng * Math.cos(Math.toRadians(this.angulo));
        this.mudaPos(nome, this.x, this.y);
        this.fora_da_tela();
    }
 
    public Shape getShape(){
    
        return this.nave;
    }
    
    public static int getVida(){
        
        return vida;
    }
    
    public static void perdeVida(){
        
        if(tomouDano == false){
            
            if(vida > 0){
                
                vida--;
                ListadeAster.setStreak(0);
                tiroNormal = true;
                tomouDano = true;
            }
            else{
                
                perdeu = true;
            }
        }
    }
    
    public static void ganhaVida(){
        
        vida++;
    }
    
    public static boolean getPerdeu(){
        
        return perdeu;
    }
    
    public static void setTiroNormal(boolean stat){
        
       tiroNormal = stat;
    }
    
}